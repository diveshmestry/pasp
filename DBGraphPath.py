import pygraphviz as pgv
from pygraphviz import *
from collections import deque


def colorNode(graph,node,colorCode):
	colorList = ["yellow","green","brown","pink","orange","red","purple","blueviolet","skyblue","cyan","aqua","magenta"]
	nodeToColor = graph.get_node(node)
	if colorCode == 0:
		nodeToColor.attr['style'] = 'filled'
		nodeToColor.attr['fillcolor'] = colorList[0]
	elif colorCode == 1:
		nodeToColor.attr['style'] = 'filled'
		nodeToColor.attr['fillcolor'] = colorList[1]
	elif colorCode == 2:
		nodeToColor.attr['style'] = 'filled'
		nodeToColor.attr['fillcolor'] = colorList[2]
	elif colorCode == 3:
		nodeToColor.attr['style'] = 'filled'
		nodeToColor.attr['fillcolor'] = colorList[3]
	elif colorCode == 4:
		nodeToColor.attr['style'] = 'filled'
		nodeToColor.attr['fillcolor'] = colorList[4]
	elif colorCode == 5:
		nodeToColor.attr['style'] = 'filled'
		nodeToColor.attr['fillcolor'] = colorList[5]
	elif colorCode == 6:
		nodeToColor.attr['style'] = 'filled'
		nodeToColor.attr['fillcolor'] = colorList[6]
	elif colorCode == 7:
		nodeToColor.attr['style'] = 'filled'
		nodeToColor.attr['fillcolor'] = colorList[7]
	elif colorCode == 8:
		nodeToColor.attr['style'] = 'filled'
		nodeToColor.attr['fillcolor'] = colorList[8]
	elif colorCode == 9:
		nodeToColor.attr['style'] = 'filled'
		nodeToColor.attr['fillcolor'] = colorList[9]
	elif colorCode == 10:
		nodeToColor.attr['style'] = 'filled'
		nodeToColor.attr['fillcolor'] = colorList[10]
	elif colorCode == 11:
		nodeToColor.attr['style'] = 'filled'
		nodeToColor.attr['fillcolor'] = colorList[11]


#create path to root 
def createPathToRoot(graph,nodessearched,rootnode,rootlist):
	currentRoot = nodessearched[0]
	while currentRoot != rootnode:
		#get in neighbors of current node that are in the rootlist
		currentRootNeighbors = getOutNeighbors(graph,currentRoot)
		for i in range(len(currentRootNeighbors)):
			if currentRootNeighbors[i] in rootlist:
				nodessearched.append(currentRootNeighbors[i])
				break
		currentRoot = nodessearched[-1]
	print "path made"


#gets leaves in the subgraph
def getSubLeaves(graph):
	leaves = []
	for node in graph.nodes():
		if len(graph.out_neighbors(node)) == 0:
			leaves.append(node)
	return leaves



#finds all paths from root to a leaf for searching outwards
def findAPathToNodeOut(graph, start, end,path=[]):
	path = path + [start]
	if start == end:
		return [path]
	if not graph.get_node(start):
		return []
	for node in graph.out_neighbors(start):
		if node not in path:
			newpaths = findAPathToNodeOut(graph, node, end, path)
			if newpaths:
				return newpaths
	return []


#finds a path 
def findAPathToNodeIn(graph, start, end,path=[]):
	path = path + [start]
	if start == end:
		return [path]
	if not graph.get_node(start):
		return []
	for node in graph.in_neighbors(start):
		if node not in path:
			newpaths = findAPathToNodeIn(graph, node, end, path)
			if newpaths:
				return newpaths
	return []




#generates a list of paths from root to every leaf
def findEveryPath(graph,root):	
	print "finding every path"
	everyPath = []
	leaves = getSubLeaves(graph)
	for leaf in leaves:
		paths = findAllPaths(graph,root,leaf)
		for i in range(len(paths)):
			everyPath.append(paths[i])
	return everyPath
	


# gives a score to every path generated in the subgraph
def scorePaths(graph,paths):
	scores = []
	for i in range(len(paths)):
		score = random.randint(0,10)
		paths[i].append(score)
	return paths		


# identifies highest score
def findHighScorePath (paths):
	biggest = -1
	for path in paths:
		if path[-1] > biggest:
			biggest = path[-1]
	return biggest


# colors nodes corresponding to path with highest score
def colorHighScorePath(graph,paths,biggest):
	for path in paths:
		if path[-1] == biggest:
			for node in path[:-1]:
				colorANode(graph,node)


def findAPathFromRootToEveryColoredNode(graph,root,colorednodes,direction):
	everyPathForColoredNodes = []
	for node in colorednodes:
		if direction == "out":
			aColorNodePaths = findAPathToNodeOut(graph,graph.get_node(root),graph.get_node(node))
		if direction == "in":
			aColorNodePaths = findAPathToNodeOut(graph,graph.get_node(node),graph.get_node(root))
		for path in aColorNodePaths:
			everyPathForColoredNodes.append(path)
	return everyPathForColoredNodes


#writes paths to a text file
def writePathsToTextFile(paths,outputfilename):
	print "writing paths to textfile"
	#load deBruijn graph to search
	#pathfilename = raw_input("name of path file :")
	try:
		f = open("../output/" + outputfilename + '.paths.txt','a')
		ID = 0
		for path in paths:
			f.write(str(ID)),
			for node in path:
				f.write(" " + node)
			f.write("\n")	
			ID = ID + 1
		f.close()
		
	
	except:
		print "file couldn't be opened and or written to"
		return



def find_shortest_path(graph, start, end, path=[]):
	path = path + [start]
	if start == end:
   		return [path]
	if not graph.has_node(start):
   		return []
	shortest = []
	for node in graph.out_neighbors(start):
		if node not in path:
			newpath = find_shortest_path(graph, node, end, path)
			if newpath:
				if not shortest or len(newpath) < len(shortest):
					shortest = newpath
	return shortest



