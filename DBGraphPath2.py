import pygraphviz as pgv
from pygraphviz import *
from collections import deque





#finds all paths from root to a leaf searching outwards
def findAPathToNodeOut(graph, start, end,path=[]):
	path = path + [start]
	if start == end:
		return [path]
	if not graph.get_node(start):
		return []
	for node in graph.out_neighbors(start):
		if node not in path:
			newpaths = findAPathToNodeOut(graph, node, end, path)
			if newpaths:
				return newpaths
	return []

#finds a path from root to every colored node
def findAPathFromRootToEveryColoredNode(graph,root,colorednodes,direction):
	PathForColoredNodes = []
	for node in colorednodes:
		if direction == "out":
			aColorNodePath = findAPathToNodeOut(graph,graph.get_node(root),graph.get_node(node))
		if direction == "in":
			aColorNodePath = findAPathToNodeOut(graph,graph.get_node(node),graph.get_node(root))
		for path in aColorNodePath:
			PathForColoredNodes.append(path)
	return PathForColoredNodes


#finds every path from root to node 
def findEveryPathFromRootToANode(graph, start, end,path=[]):
	path = path + [start]
	if start == end:
		return [path]
	if not graph.get_node(start):
		return []
	paths = []
	for node in graph.out_neighbors(start):
		if node not in path:
			newpaths = findEveryPathFromRootToANode(graph, node, end,path)
			for newpath in newpaths:
				paths.append(newpath)
	return paths

#find every path from root to every colored node
def findEveryPathFromRootToEveryNode(graph,root,colorednodes,direction):
	everyPathForColoredNodes = []
	for node in colorednodes:
		if direction == "out":
			aColorNodePaths = findEveryPathFromRootToANode(graph, graph.get_node(root), graph.get_node(node))
		if direction == "in":
			aColorNodePaths = findEveryPathFromRootToANode(graph, graph.get_node(node), graph.get_node(root))
		for path in aColorNodePaths:
			everyPathForColoredNodes.append(path)
	return everyPathForColoredNodes
	


#writes paths to a text file
def writePathsToTextFile(paths,outputfilename):
	print "writing paths to textfile"
	#load deBruijn graph to search
	#pathfilename = raw_input("name of path file :")
	try:
		f = open("../PASPoutput/" + outputfilename + '.paths.txt','a')
		ID = 0
		for path in paths:
			f.write(str(ID)),
			for node in path:
				f.write(" " + node)
			f.write("\n")	
			ID = ID + 1
				
		f.close()
		
	
	except:
		print "file couldn't be opened and or written to"
		return






#TESTING
#G = pgv.AGraph("/Users/dmestry/Desktop/polyAproject/Software/testgraph.dot")
#if G:
#	print "graph loaded"
#	paths = findEveryPathFromRootToEveryNode(G,"1",["20","30","40"],"out")
#	print 
#	print paths[0]
#	writePathsToTextFile(paths,"nov6test.txt")
	
#else:
#	print "graph not loaded"
	



 

#print findAPathFromRootToEveryColoredNode(G,"1",["20","30","40"],"out")

print "done"


