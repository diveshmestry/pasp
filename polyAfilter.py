import Bio
from Bio.Seq import Seq
from Bio import SeqIO



def createlist0():
	signal1 = "AATAAA"
	signal2 = "ATTAAA"
	signal3 = "TATAAA"
	signal4 = "AGTAAA"
	signal5 = "AAGAAA"
	signal6 = "AATATA"
	signal7 = "AATACA"
	signal8 = "CATAAA"
	signal9 = "GATAAA"
	signal10 = "AATGAA"
	signal11 = "ACTAAA"
	signal12 = "AATAGA"
	signal13 = "TGTAA"
	signal14 = "TGTAC"
	signal15 = "TGTAG"
	signal16 = "TGTAT"
	signalList = []
	#store signals in a list
	signalList.append(signal1)
	signalList.append(signal2)
	signalList.append(signal3)
	signalList.append(signal4)
	signalList.append(signal5)
	signalList.append(signal6)
	signalList.append(signal7)
	signalList.append(signal8)
	signalList.append(signal9)
	signalList.append(signal10)
	signalList.append(signal11)
	signalList.append(signal12)
	signalList.append(signal13)
	signalList.append(signal14)
	signalList.append(signal15)
	signalList.append(signal16)
	#add signal to list here
	return signalList


def createlist1():
	# must reverse complement signals if you start searching from polyT node
	signal1 = "TTTATT"  
	signal2 = "TTTAAT"
	signal3 = "TTTATA"
	signal4 = "TTTACT"
	signal5 = "TTTCTT"
	signal6 = "TATATT"
	signal7 = "TGTATT"
	signal8 = "TTTATG"
	signal9 = "TTTATC"
	signal10 = "TTCATT"
	signal11 = "TTTAGT"
	signal12 = "TCTATT"
	signal13 = "TTACA"
	signal14 = "GTACA"
	signal15 = "CTACA"
	signal16 = "ATACA"
	signalList = []
	#store signals in a list
	signalList.append(signal1)
	signalList.append(signal2)
	signalList.append(signal3)
	signalList.append(signal4)
	signalList.append(signal5)
	signalList.append(signal6)
	signalList.append(signal7)
	signalList.append(signal8)
	signalList.append(signal9)
	signalList.append(signal10)
	signalList.append(signal11)
	signalList.append(signal12)
	signalList.append(signal13)
	signalList.append(signal14)
	signalList.append(signal15)
	signalList.append(signal16)
	return signalList


def forwardOrReverseComp():
	try:
		f = raw_input("forward(0) or Reverse Complement(1) signals:")
		if f == '0':
			fsignallist = createlist0()
			return fsignallist
		if f == '1':
			rcsignallist = createlist1()
			return rcsignallist
	except:
		print "please enter 0 or 1"



#finds reads with high A percentages
def checkforAtailRead(curRecordID,curRecordLength,taillist,record):
	cutoff = 97
	Acount = record.seq.count('A')
	Apercent = float((Acount/curRecordLength)*100)
	if Apercent > cutoff:
		taillist.append(curRecordID + '+')
		taillist.append(curRecordID + '-')

#finds reads with high T percentages
def checkforTtailRead(curRecordID,curRecordLength,taillist,record):
	cutoff = 97                       
	Tcount = record.seq.count('T')
	Tpercent = float((Tcount/curRecordLength)*100)
	if Tpercent > cutoff:
		taillist.append(curRecordID + '+')
		taillist.append(curRecordID + '-')



# asks user to generate list of polyA nodes or polyT nodes 
def polyAOrPolyT():
	try:
		AorT = raw_input("PolyA(0) or PolyT(1) root nodes:")
		return AorT
	except:
		print "please enter 0 or 1"


def filterReadsForSignals(signalList,aort):
	print "enter the path to create a list of reads with signals"
	f = raw_input("path of reads:")
	try:
		reads = open(f,"r+")
		print "reads open"
	except:
		print "couldn't open file"
		return 
	if reads:
		RAM = []
		tails = []
		
		#input file including file path and filetype
		for index,record in enumerate(SeqIO.parse(reads,"fasta")):
			# record id and length
			curRecordID = record.id
			curRecordLength = float(len(record.seq))
			
			## checks reads for start
			if aort == '0':
				checkforAtailRead(curRecordID,curRecordLength,tails,record)
			if aort == '1':
				checkforTtailRead(curRecordID,curRecordLength,tails,record)
			
			#check only records greater than 1000 bp long
			if curRecordLength > 500:
				#seq transcription
				forWard = record.seq
				#reverse read
				reverse = record.seq[::-1]
				
				forwardSignalCount = []
				reverseSignalCount = []
				
				forwardSignalPositions = []
				reverseSignalPositions = []
				
				#count number of signals in forward and reverse reads
				for i in range(len(signalList)):
					forwardSignalCount.append(forWard.count(signalList[i]))
					#reverse read
					reverseSignalCount.append(reverse.count(signalList[i]))	
				# find the positions of each signal
				for i in range(len(forwardSignalCount)):
					#do same for reverseSignalCount
					if forwardSignalCount[i] is 0:
						forwardSignalPositions.append(0)
					if forwardSignalCount[i] is 1:
						forwardSignalPositions.append(forWard.find(signalList[i]))
					if forwardSignalCount[i] > 1:
						#find most frequent signal
						#find the position of the right most occurring signal
						forwardSignalPositions.append(forWard.rfind(signalList[i]))
				#calculate the distance from the end of each signal
				# find the positions of each signal reverse
				for i in range(len(reverseSignalCount)):
					if reverseSignalCount[i] is 0:
						reverseSignalPositions.append(0)
					if reverseSignalCount[i] is 1:
						reverseSignalPositions.append(reverse.find(signalList[i]))
					if reverseSignalCount[i] > 1:
						#find most frequent signal
						#find the position of the right most occurring signal
						reverseSignalPositions.append(reverse.rfind(signalList[i]))
				#calculate the distance from the end of each signal
				DfromEnd = []
				for i in range(len(forwardSignalPositions)):
					if forwardSignalPositions[i] is 0:
						DfromEnd.append(0)
					else:
						DfromEnd.append(curRecordLength - float(forwardSignalPositions[i]))
				DfromEndRev = []
				for i in range(len(reverseSignalPositions)):
					if reverseSignalPositions[i] is 0:
						DfromEndRev.append(0)
					else:
						DfromEndRev.append(curRecordLength - float(reverseSignalPositions[i]))
				#keep record id's of contigs that have a signal within 50 bp of the end and a UGUAN element
				recordInfo = [] 
				for i in range(len(DfromEnd)):
					if  0 < DfromEnd[i] < 50:
						if DfromEnd[12] > 0 or DfromEnd[13] > 0 or DfromEnd[14] > 0 or DfromEnd[15] > 0:
							if i < 12:
								recordInfo.append(curRecordID + "+"),
								recordInfo.append(i)
								break
				reverserecordInfo =[]	
				for i in range(len(DfromEndRev)):
					if 0 < DfromEndRev[i] < 50:
						if DfromEndRev[12] > 0 or DfromEndRev[13] > 0 or DfromEndRev[14] > 0 or DfromEndRev[15] > 0:
							if i < 12:	
								reverserecordInfo.append(curRecordID + "-"),
								reverserecordInfo.append(i)
								break
				if recordInfo:
					RAM.append(recordInfo)
				if reverserecordInfo:
					RAM.append(reverserecordInfo)
		
		print"found ",len(RAM) , "reads with signals"
		print "found" , len(tails), "reads that are potential start points"
		reads.close()
		return RAM,tails
	else:
		print "no reads"
		return
	

 
#testing 
#contigfile = "/Users/dmestry/Desktop/polyAproject/Software/PASP/datasets/MM0472/MM0472-contigs.fa"
#print contigfile

#readsfile= "/Users/dmestry/Desktop/polyAproject/Software/PASP/datasets/MM0472/MM0472-4.fa"
#print readsfile

#cfile = open(contigfile,"r")
#rfile = open(readsfile,"r")

#taillist =[]
#taillist2 = []
#for index,record in enumerate(SeqIO.parse(cfile,"fasta")):
#	curRecordID = record.id
#	curRecordLength = float(len(record.seq))
#	seq = record.seq
	
#	checkforTtailRead(curRecordID,curRecordLength,taillist2,record)
	
	
#print taillist2

