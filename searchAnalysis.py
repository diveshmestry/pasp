

def findRootWithMostNearBySignals(rootsAndCounts,highcount):
	highcountroots = []
	for i in range(len(rootsAndCounts)):
		if rootsAndCounts[i][1] == highcount:
			highcountroots.append(rootsAndCounts[i])
	return highcountroots
		


def signalcounts(rootsAndCounts):
	countlist = []
	for i in range(len(rootsAndCounts)):
		countlist.append(rootsAndCounts[i][1])
	return countlist

def findHighestCount(countlist):
	highcount = -1
	for count in countlist:
		if count > highcount:
			highcount = count
	return highcount
	



def createRootandSignalCountEntry(root,signalcount,colorednodelist):
	entry =  [root,signalcount,colorednodelist]
	return entry


def writeSearchMatrixToTextFile(matrix,outputfilename):
	print "writing search data to textfile"
	
	#pathfilename = raw_input("name of path file :")
	try:
		f = open('../PASPoutput/' + outputfilename + 'scm.txt','w')
		for row in matrix:
			f.write(str(row))
			f.write("\n")
		f.close()
	
	except:
		print "file couldn't be opened and or written to"
		return
	


def findLengthOfFile(fname):
	with open(fname) as f:
		for i, l in enumerate(f):
			 pass
		return i + 1


def askFileName():
	try:
		f = raw_input("Please enter a file name:")
		return f
	except:
		print "please enter name"



