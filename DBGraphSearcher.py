import pygraphviz as pgv
from pygraphviz import *
from collections import deque

#loads a graph in dot format
def loadGraph():
	#load deBruijn graph to search
	f = raw_input("path of Graph:")
	try:
		G=pgv.AGraph(f)
		print "graph loaded"
		return G
	except:
		return
	


#Colors a given node 
def colorNode(graph,node,colorCode):
	colorList = ["yellow","green","brown","pink","orange","red","purple","blueviolet","skyblue","cyan","aqua","magenta"]
	nodeToColor = graph.get_node(node)
	if colorCode == 0:
		nodeToColor.attr['style'] = 'filled'
		nodeToColor.attr['fillcolor'] = colorList[0]
	elif colorCode == 1:
		nodeToColor.attr['style'] = 'filled'
		nodeToColor.attr['fillcolor'] = colorList[1]
	elif colorCode == 2:
		nodeToColor.attr['style'] = 'filled'
		nodeToColor.attr['fillcolor'] = colorList[2]
	elif colorCode == 3:
		nodeToColor.attr['style'] = 'filled'
		nodeToColor.attr['fillcolor'] = colorList[3]
	elif colorCode == 4:
		nodeToColor.attr['style'] = 'filled'
		nodeToColor.attr['fillcolor'] = colorList[4]
	elif colorCode == 5:
		nodeToColor.attr['style'] = 'filled'
		nodeToColor.attr['fillcolor'] = colorList[5]
	elif colorCode == 6:
		nodeToColor.attr['style'] = 'filled'
		nodeToColor.attr['fillcolor'] = colorList[6]
	elif colorCode == 7:
		nodeToColor.attr['style'] = 'filled'
		nodeToColor.attr['fillcolor'] = colorList[7]
	elif colorCode == 8:
		nodeToColor.attr['style'] = 'filled'
		nodeToColor.attr['fillcolor'] = colorList[8]
	elif colorCode == 9:
		nodeToColor.attr['style'] = 'filled'
		nodeToColor.attr['fillcolor'] = colorList[9]
	elif colorCode == 10:
		nodeToColor.attr['style'] = 'filled'
		nodeToColor.attr['fillcolor'] = colorList[10]
	elif colorCode == 11:
		nodeToColor.attr['style'] = 'filled'
		nodeToColor.attr['fillcolor'] = colorList[11]
		


#colors the root
def colorRoot(graph,root):
	nodeToColor = graph.get_node(root)
	nodeToColor.attr['style'] = 'filled'
	nodeToColor.attr['fillcolor'] = "grey"


#colors a node (light blue)
def colorANode(graph,node):
	nodeToColor = graph.get_node(node)
	if nodeToColor.attr['style'] != 'filled':
		nodeToColor.attr['style'] = 'filled'
		nodeToColor.attr['fillcolor'] = "lightblue"


#checks a node to see if its in the signal list, adds node to a list of nodes checked
def checkNode(graph,node,signalList,subgraphnodes,newgraph):
	
	try:
		nodeToCheck = graph.get_node(node)
		#flag node as checked
		nodeToCheck.attr['comment'] = "checked"
		#color new graph node
		#add this node and its children to the subgraph nodes and draw on to new graph	
		addToSubgraphNodes(graph,newgraph,subgraphnodes,node)
		
		nodeToColor = newgraph.get_node(node)
		for i in range(len(signalList)):
			if nodeToColor == signalList[i][0]:
				colorCode = signalList[i][1] 
				colorNode(newgraph,nodeToColor,colorCode)
		
	except:
		print "couldn't check node"
		print node, "is not in graph"
	

	
# returns the out neighbors of a given  node
def getOutNeighbors(graph,node):
    return graph.out_neighbors(node)

# returns the in neighbors of a given node
def getInNeighbors(graph,node):
    return graph.in_neighbors(node)


# adds the in neighbors to the neighbors to search if they haven't been checked and aren't in the search list
def addInNeighborsToSearchList(searchlist,inneighbors):
    for i in range(len(inneighbors)):
		if inneighbors[i].attr['comment'] != "checked" and inneighbors[i] not in searchlist:
			searchlist.append(inneighbors[i])

# adds the outneighbors of a node to the list of nodes to searched as long as they haven't been checked yet
def addOutNeighborsToSearchList(searchlist,outneighbors):
    for i in range(len(outneighbors)):
		if outneighbors[i].attr['comment'] != "checked" and outneighbors[i] not in searchlist:
			searchlist.append(outneighbors[i])



# adds a node to the list of nodes that have been checked
def addToSubgraphNodes(graph,newGraph,subGraphnodes,node):
	subGraphnodes.append(node)
	for i in range(len(graph.out_edges(node))):
			addEdge(graph,newGraph,graph.out_edges(node)[i][0],graph.out_edges(node)[i][1])
	



#checks nodes searched for colored nodes, returns a count of colored nodes
def checknodesSearched(graph,nodessearched):
	colorList = ["yellow","green","brown","pink","orange","red","purple","blueviolet","skyblue","cyan","aqua","magenta"]
	coloredNodeCount = 0
	for i in range(len(nodessearched)):
		currentnode = graph.get_node(nodessearched[i]) 
		if currentnode.attr['fillcolor'] in colorList:
			coloredNodeCount = coloredNodeCount + 1
			
	return coloredNodeCount



#searches a specified graph area
def searchGraphAreaForSignals(graph,node,signallist,newgraph):	
	nodeCounter = 0
	#make a list of nodes to search
	SearchList = deque()
	#make a list of nodes searched to be included in the subgraph
	subgraphnodes = []
	#add node to SearchList
	SearchList.append(node)
	# keep searching the graph as long as the search list isn't empty and the number of nodes 
	# checked isn't 500
	while len(SearchList) != 0 and nodeCounter < 500:
		#remove first node to be checked, first node in list
		currentnode = SearchList.popleft()
		#print "the current node being checked is", currentnode
		#check first node
		checkNode(graph,currentnode,signallist,subgraphnodes,newgraph)
		#increase the counter of how many nodes have been checked
		nodeCounter = nodeCounter + 1
		# add current nodes neighbors to search list only if they haven't been checked
		addOutNeighborsToSearchList(SearchList,getOutNeighbors(graph,currentnode))
	return subgraphnodes




#searches a given graph and creates the new graph
def searchWholeGraph(graph,rootlist,RAM,newgraph,masterRootList):
	
	first = getOutNeighbors(graph,rootlist[0])
	coloredNodeList = []
	####
	#check root for neighbors
	if len(first) > 0:
		#search graph for signal using first root
		nodesSearched = searchGraphAreaForSignals(graph,rootlist[0],RAM,newgraph)
		
		#count how many colored nodes there were
		numOfColoredNodes = checknodesSearched(newgraph,nodesSearched)
		
		if numOfColoredNodes > 0:
			
			for node in nodesSearched:
				Node = newgraph.get_node(node)
				if Node.attr['style'] == 'filled':
					coloredNodeList.append(Node)
			return numOfColoredNodes,nodesSearched,coloredNodeList
		else:
			#try searching from another place in the nodesSearched 
			return numOfColoredNodes,nodesSearched,coloredNodeList.append(-1)
	else:
		return -1,-1,coloredNodeList.append(-1)

	
#finds a node and colors it, making it represent the root
def findRoot(graph,node):
	try:
		root = graph.get_node(node)
		colorRoot(graph,root)
		return root
	except:
		return -1
		


#adds a node to a given graph
def addNode(graph,node):
	try:
		graph.add_node(node)
	except:
		print "could not add node"
		


#adds an edge in the new graph if there is an edge between a and b in the orignal graph
def addEdge(graph,newGraph,a,b):
	if graph.has_edge(a,b):
		newGraph.add_edge(a,b)



#checks that there is a graph to search in, nodes to look for, a root to start looking from, and initiates the search
def prepforSearch(RAM,G,H,Aroot):
	if RAM:
		if G:
			rootlist = deque()
			root = findRoot(G,Aroot)
			if root != -1:
				rootlist.append(root)
				masterRootList = []
				masterRootList.append(root)
				numOfColoredNodes,nodesSearched,coloredNodeList = searchWholeGraph(G,rootlist,RAM,H,masterRootList)
			else:
				numOfColoredNodes = -1
			
			#if there were colored nodes
			if numOfColoredNodes > 0:
				return numOfColoredNodes,coloredNodeList
				
			else:
				#print "try searching for signals near another potential polyA tail node"
				return -1,coloredNodeList
				
		else:
			print "graph couldn't be loaded"
			print "quitting "
			return -1,coloredNodeList
	else:
		print"list of reads with signals couldn't be compiled"	
		print "done for now"
		return -1,coloredNodeList



