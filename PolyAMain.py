import pygraphviz as pgv
from pygraphviz import *
from collections import deque
import Bio
from Bio.Seq import Seq
from Bio import SeqIO
import subprocess
from subprocess import *

import DBGraphSearcher
from DBGraphSearcher import *

import polyAfilter
from polyAfilter import *
import DBGraphPath2
import searchAnalysis

import DBGraphSearcherIn
from DBGraphSearcherIn import *
import MergePaths
from MergePaths import *


#features 
###
#asks users for forward or reverse complement signals 
#asks users for polyA or PolyT starting point
#asks users to search in or outwards from start point
#searches graph for specified signals from specified start points
#records information about signals found in signal count matrix (scm)
#creates a second graph with start point as root and specified signals near it
#finds A path from starting point to every node with signal in the second acyclic graph
#writes paths to a text file
#clears this second graph everytime it starts searching from a new point


def askOutputFilename():
	try:
		f = raw_input("Please enter an output file name:")
		return f
	except:
		print "please enter 0 or 1"
	


def askSearchInOrOut():
	try:
		f = raw_input("Search out from root node(0) or in to root node(1):")
		return f
	except:
		print "please enter 0 or 1"


def searchIn(outputfilename):
	scm = []
	direction = "in"
	#signalcountmatrix.append(["Start point","# of Colored Nodes","colored node ID's"])
	print "checking graph going inwards"
	for i in range(len(tails)):
		if G.has_node(tails[i]):
			aRoot = tails[i]
			colornodes,coloredNodeList = DBGraphSearcherIn.prepforSearch(RAM,G,H,aRoot)
			if colornodes != -1:
				entry = searchAnalysis.createRootandSignalCountEntry(aRoot,colornodes,coloredNodeList)
				scm.append(entry)
				aH=H.acyclic(copy=True) 
				paths =  DBGraphPath2.findEveryPathFromRootToEveryNode(aH,aRoot,coloredNodeList,direction)
				if paths:
					DBGraphPath2.writePathsToTextFile(paths,outputfilename)
				aH.clear()
				H.clear()
		else:
			print "couldn't find a polyA tail node"
	if scm:
		searchAnalysis.writeSearchMatrixToTextFile(scm,outputfilename)
		print "..done"
		signalcounts = searchAnalysis.signalcounts(scm)
	else:
		print "no signals found" 



def searchOut(outputfilename):
	scm = []
	direction = "out"
	#signalcountmatrix.append(["Start point","# of Colored Nodes","colored node ID's"])
	print "checking graph outwards"
	for i in range(len(tails)):
		if G.has_node(tails[i]):
			aRoot = tails[i]
			colornodes,coloredNodeList = DBGraphSearcher.prepforSearch(RAM,G,H,aRoot)
			if colornodes != -1:
				entry = searchAnalysis.createRootandSignalCountEntry(aRoot,colornodes,coloredNodeList)
				scm.append(entry)
				aH=H.acyclic(copy=True) 
				paths =  DBGraphPath2.findEveryPathFromRootToEveryNode(aH,aRoot,coloredNodeList,direction)
				if paths:
					DBGraphPath2.writePathsToTextFile(paths,outputfilename)
				
				aH.clear()
				H.clear()
		else:
			print "couldn't find a polyA tail node"
	
	if scm:
		print "..done"
		searchAnalysis.writeSearchMatrixToTextFile(scm,outputfilename)
		signalcounts = searchAnalysis.signalcounts(scm)
		
	else:
		print "no signals found"


print "starting up"



#create second graph
H = pgv.AGraph(strict = True, directed=True)

#create signallist
signallist = polyAfilter.forwardOrReverseComp()
AorT = polyAfilter.polyAOrPolyT()

#ask to search inwards or outwards
inorout = askSearchInOrOut()

#create read analysis matrix and list of polyA or polyT nodes
RAM,tails = polyAfilter.filterReadsForSignals(signallist,AorT)

#ask for output files
outputfile = askOutputFilename()


#load de bruijn graph
G = DBGraphSearcher.loadGraph()



if inorout == "0":
	searchOut(outputfile)
	pathfilename = outputfile + '.paths.txt'
	MergePaths.callMergeContigs(pathfilename) 
	print "done"

elif inorout == "1":
	searchIn(outputfile)
	pathfilename = outputfile + '.paths.txt'
	MergePaths.callMergeContigs(pathfilename)
	print "done"

else:
	print "please try again"	

